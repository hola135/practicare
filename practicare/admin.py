from django.contrib import admin

from .models import titular
admin.site.register (titular)

from .models import auto
admin.site.register (auto)

# Register your models here.
