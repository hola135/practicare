from django.db import models

class titular (models.Model):
    id=models.BigAutoField(primary_key=True)
    apellido=models.CharField(max_length=30)
    nombre=models.CharField(max_length=30)
    telefono=models.IntegerField()
    direccion=models.CharField(max_length=30)
    mail=models.CharField(max_length=30)
    def __str__(self):
        texto='{} {}'.format(self.apellido,
        self.apellido,
        self.nombre,
        self.telefono,
        self.direccion,
        self.mail)
        return texto

class auto (models.Model):
    titular=models.ForeignKey(titular, on_delete=models.CASCADE)
    marca=models.CharField(max_length=30)
    modelo=models.CharField(max_length=30)
    km=models.IntegerField()
    color=models.CharField(max_length=30)
    cantidaddepuertas=models.IntegerField()
    añodelmodelo=models.DateField()
    cantidaddetitularesanteriores=models.IntegerField()
    estado=models.CharField(max_length=30)
    modalidaddeingreso=models.CharField(max_length=30)
    def __str__(self):
        texto='{} {}'.format(self.marca,
        self.titular,
        self.modelo,
        self.km,
        self.color,
        self.cantidaddepuertas,
        self.añodelmodelo,
        self.estado,
        self.modalidaddeingreso)
        return texto

# Create your models here.
