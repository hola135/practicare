from django.apps import AppConfig


class PracticareConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'practicare'
