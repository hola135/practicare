FROM python:3.10

    RUN mkdir /practicare

    WORKDIR /practicare

    ADD . /practicare

    RUN pip install --upgrade pip

    RUN pip install -r requerimientos.txt